#ifndef PROCPS_PROC_PROCIO_H
#define PROCPS_PROC_PROCIO_H

#include "procps.h"

EXTERN_C_BEGIN

extern FILE *fprocopen(const char *, const char *);

EXTERN_C_END
#endif
